import 'package:flutter/material.dart';
import 'package:flutter_demo/model/transaction.dart';
import 'package:flutter_demo/widgets/transaction_item.dart';

class TransactionList extends StatelessWidget {
  const TransactionList(this.transactions, this.delete);

  final List<Transaction> transactions;
  final Function delete;

  @override
  Widget build(BuildContext context) {

    const TextStyle textstyleLabel = TextStyle(color: Colors.black87);
    const String labelText = 'No data';
    return transactions.isEmpty
        ? LayoutBuilder(
            builder: (BuildContext ctx, BoxConstraints constraints) {
            return Container(
              margin: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  const Text(labelText, style: textstyleLabel)
                ],
              ),
            );
          })
        : ListView(
            children: transactions
                .map((Transaction e) => TransactionItem(
                    key: ValueKey<String>(e.id.toString()),
                    transaction: e,
                    delete: delete))
                .toList(),
          );
  }
}
