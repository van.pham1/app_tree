import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_demo/widgets/chart.dart';
import 'package:flutter_demo/widgets/transaction_list.dart';
import 'package:flutter_demo/widgets/transaction_new.dart';

import 'model/transaction.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Personal Expenses',
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  List<Transaction> get _recentTransactions {
    return _userTransactions.where((Transaction index) {
      return index.date.isAfter(
        DateTime.now().subtract(
          const Duration(days: 7),
        ),
      );
    }).toList();
  }

  void _deleteTransaction(String id) {
    setState(() {
      _userTransactions.removeWhere((Transaction index) => index.id == id);
    });
  }

  void _startAddNewTransaction(BuildContext ctx) {
    showModalBottomSheet<void>(context: ctx, builder: (BuildContext context) {
        return GestureDetector(
          child: TransactionNew(_addNewTransaction),
          behavior: HitTestBehavior.opaque,
        );
      }
    );
  }

  void _addNewTransaction(
      String txTitle, double txAmount, DateTime chosenDate) {
    final Transaction newTx = Transaction(
      title: txTitle,
      amount: txAmount,
      date: chosenDate,
      id: DateTime.now().toString(),
    );

    setState(() {
      _userTransactions.add(newTx);
    });
  }

  final List<Transaction> _userTransactions = <Transaction>[];
  @override
  Widget build(BuildContext context) {
    final MediaQueryData mediaQuery = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1.0,
        brightness: Brightness.light,
        title: const Text('Personal Expenses',
            style: TextStyle(color: Colors.black, fontSize: 15)),
        actions: <Widget>[
          IconButton(
              icon: const Icon(
                Icons.add,
                color: Colors.black,
              ),
              onPressed: () => _startAddNewTransaction(context))
        ],
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
                height: 200,
                child: Chart(_recentTransactions)),
            Container(

              height: (mediaQuery.size.height -
                  mediaQuery.padding.top - kToolbarHeight) *
                  0.7,
              child: TransactionList(_userTransactions, _deleteTransaction),)
          ],
        ),
      ),
    );
  }
}
